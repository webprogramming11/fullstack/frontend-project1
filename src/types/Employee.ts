export default interface Product {
  id?: number;
  name: string;
  role: string;
  phone: string;
  salary: number;
}
