import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/service/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const dialog = ref(false);
  const delDialig = ref(false);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: " ", price: 0 });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: " ", price: 0 };
    }
  });
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to load resource.");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to save product");
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  async function deleteProduct() {
    loadingStore.isLoading = true;
    try {
      await productService.deleteProduct(delId);
      delId = -1;
      delDialig.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Failed to delete product");
    }
    loadingStore.isLoading = false;
  }
  let delId = 0;
  function confirmDelete(id: number) {
    delDialig.value = true;
    delId = id;
  }

  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    delDialig,
    confirmDelete,
  };
});
